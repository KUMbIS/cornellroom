﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CornishRoom
{
    public partial class Form1 : Form
    {
        public List<Figure> scene = new List<Figure>();
        public List<Light> lights;   // список источников света
        public Color[,] pixels_color;                    // цвета пикселей для отображения на pictureBox
        public Point3D[,] pixels;
        public Point3D cameraPoint;
        public Point3D up_left, up_right, down_left, down_right;
        public int h, w;
		float b = 0f;
        bool light1 = true;
        bool light2 = false;
		bool mirror = true;

        public Form1()
        {
            InitializeComponent();
            cameraPoint = new Point3D();
            up_left = new Point3D();
            up_right = new Point3D();
            down_left = new Point3D();
            down_right = new Point3D();
            h = pictureBox1.Height;
            w = pictureBox1.Width;
            pictureBox1.Image = new Bitmap(w, h);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lights = new List<Light>();
            BuildScene();
            BackwardRayTracing();
            for (int i = 0; i < w; ++i){
                for (int j = 0; j < h; ++j)
                {
                    (pictureBox1.Image as Bitmap).SetPixel(i, j, pixels_color[i, j]);
                }
                pictureBox1.Invalidate();
            }
        }

        public void BuildScene()
        {
			pictureBox1.Image = new Bitmap(w, h);
			//сама комната
			Figure room = Figure.get_Cube(10);
            up_left = room.sides[0].getPoint(0);
            up_right = room.sides[0].getPoint(1);
            down_right = room.sides[0].getPoint(2);
            down_left = room.sides[0].getPoint(3);

            //ставим камеру
            Point3D normal = Side.norm(room.sides[0]);                            // нормаль стороны комнаты
            Point3D center = (up_left + up_right + down_left + down_right) / 4;   // центр стороны комнаты
            cameraPoint = center + normal * 11;

            //красим стены
            room.SetPen(new Pen(Color.Gray));
            room.sides[0].drawing_pen = new Pen(Color.Green);
            room.sides[1].drawing_pen = new Pen(Color.Yellow);
            room.sides[2].drawing_pen = new Pen(Color.Red);
            room.sides[3].drawing_pen = new Pen(Color.Blue);
            room.fMaterial = new Material(0, 0.05f, 0.7f);

            //добавляем источники света
            if (light1)
            {
                Light l1 = new Light(new Point3D(0f, 2f, 4.8f), new Point3D(1f, 1f, 1f));//белый, посреди комнаты
                lights.Add(l1);
            }
            if (light2)
            {
                Light l2 = new Light(new Point3D(4.99f, -4.99f, 4.9f), new Point3D(1f, 1f, 1f));//белый, дальний верхний левый угол
                lights.Add(l2);
            }

            Sphere mirrorSphere = new Sphere(new Point3D(-2.5f, -2, 2.5f), 2.5f);
            mirrorSphere.SetPen(new Pen(Color.White));
            mirrorSphere.fMaterial = new Material(0.9f, 0f, 0.1f, 1f);

            Figure bigCube = Figure.get_Cube(2.8f);
            bigCube.Offset(-1.5f, 1.5f, -3.9f);//сдвиг по осям 
            bigCube.RotateAround(50, "CZ");
            bigCube.SetPen(new Pen(Color.Magenta));
            bigCube.fMaterial = new Material(0f, 0.1f, 0.7f, 1.5f);

			Figure mirrorCube;
			if (mirror)
			{
				mirrorCube = Figure.get_Cube(1.5f + b);
				mirrorCube.Offset(2.5f, 2f, -4.25f);
				mirrorCube.RotateAround(110, "CZ");
				mirrorCube.SetPen(new Pen(Color.White));
				mirrorCube.fMaterial = new Material(0.9f, 0f, 0.1f, 1.2f);
			}
			else
			{
				mirrorCube = Figure.get_Cube(1.5f + b);
				mirrorCube.Offset(2.5f, 2f, -4.25f);
				mirrorCube.RotateAround(110, "CZ");
				mirrorCube.SetPen(new Pen(Color.Cyan));
				System.Diagnostics.Debug.WriteLine(mirror);
				mirrorCube.fMaterial = new Material(0f, 0.1f, 0.7f, 1.5f);
			}

			scene.Add(room);
            scene.Add(bigCube);
            scene.Add(mirrorSphere);
			scene.Add(mirrorCube);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                light1 = true;
            else
                light1 = false;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
                light2 = true;
            else
                light2 = false;
        }

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (comboBox1.SelectedIndex == 0)
				mirror = true;
			else
				mirror = false;
			b += 0.00001f;
		}

		public void BackwardRayTracing()
        {
            // Изображние с камеры в матрице пикселей,равной размеру экрана
            GetPixels();
            /*
             Количество первичных лучей – это общее
             количество пикселей видового окна
             */
            for (int i = 0; i < w; i++)
                for (int j = 0; j < h; j++)
                {
                    // создаем луч
                    Ray r = new Ray(cameraPoint, pixels[i, j]);
                    r.start = new Point3D(pixels[i, j]);
                    Point3D color = RayTrace(r, 1000, 1); //луч,кол-во итераций, коэффициент преломления
                    if (color.x > 1.0f || color.y > 1.0f || color.z > 1.0f)
                        color = Point3D.norm(color);
                    pixels_color[i, j] = Color.FromArgb((int)(255 * color.x), (int)(255 * color.y), (int)(255 * color.z));
                }
        }

        // получение всех пикселей сцены
        public void GetPixels()
        {
            /*
             Учитывая разницу между размером комнаты и экранным отображение приводим координаты к пикселям
             */
            pixels = new Point3D[w, h];
            pixels_color = new Color[w, h];
            Point3D step_up = (up_right - up_left) / (w - 1);//отношение ширины комнаты к ширине экрана
            Point3D step_down = (down_right - down_left) / (w - 1);//отношение высоты комнаты к высоте экрана
            Point3D up = new Point3D(up_left);
            Point3D down = new Point3D(down_left);
            for (int i = 0; i < w; ++i)
            {
                Point3D step_y = (up - down) / (h - 1);
                Point3D d = new Point3D(down);
                for (int j = 0; j < h; ++j)
                {
                    pixels[i, j] = d;
                    d += step_y;
                }
                up += step_up;
                down += step_down;
            }
        }

        //видима ли точка пересечения луча с фигурой из источника света
        public bool IsVisible(Point3D light_point, Point3D hit_point)
        {
            float max_t = (light_point - hit_point).length(); //позиция источника света на луче
            Ray r = new Ray(hit_point, light_point);
            foreach (Figure fig in scene)
                if (fig.FigureIntersection(r, out float t, out Point3D n))
                    if (t < max_t && t > Figure.eps)
                        return false;
            return true;
        }

        public Point3D RayTrace(Ray r, int iter, float env)
        {
            if (iter <= 0)
                return new Point3D(0, 0, 0);
            float rey_fig_intersect = 0;// позиция точки пересечения луча с фигурой на луче
            //нормаль стороны фигуры,с которой пересекся луч
            Point3D normal = null;
            Material material = new Material();
            Point3D res_color = new Point3D(0, 0, 0);

            // пересечения с фигурами на сцене
            foreach (Figure fig in scene)
            {
                if (fig.FigureIntersection(r, out float intersect, out Point3D norm))
                    if (intersect < rey_fig_intersect || rey_fig_intersect == 0) // нужна ближайшая фигура к точке наблюдения
                    {
                        rey_fig_intersect = intersect;
                        normal = norm;
                        material = new Material(fig.fMaterial);
                    }
            }

            if (rey_fig_intersect == 0)//если не пересекается с фигурой
                return new Point3D(0, 0, 0);//Луч уходит в свободное пространство .Возвращаем значение по умолчанию

			if (Point3D.scalar(r.direction, normal) > 0)
			{
				normal *= -1;
			}

            //Точка пересечения луча с фигурой
            Point3D hit_point = r.start + r.direction * rey_fig_intersect;
            /*В точке пересечения луча с объектом строится два вторичных
              луча – один в направлении отражения (1), второй – в направлении
              источника света (2)
             */


            foreach (Light light in lights)
            {
                //цвет коэффициент принятия фонового освещения
                Point3D ambient_coef = light.color_light * material.ambient;
                ambient_coef.x = (ambient_coef.x * material.color.x);
                ambient_coef.y = (ambient_coef.y * material.color.y);
                ambient_coef.z = (ambient_coef.z * material.color.z);
                res_color += ambient_coef;
                // диффузное освещение
                if (IsVisible(light.point_light, hit_point))//если точка пересечения луча с объектом видна из источника света
                    res_color += light.Shade(hit_point, normal, material.color, material.diffuse);
            }


            /*Для отраженного луча проверяется возможность пересечения с другими объектами сцены.
                Если пересечений нет, то интенсивность и цвет отраженного луча равна интенсивности и цвету фона.
                Если пересечение есть, то в новой точке снова строится два типа лучей – теневые и отражения. 
              */
            if (material.reflection > 0)
            {
                Ray reflected_ray = r.Reflect(hit_point, normal);
                res_color += material.reflection * RayTrace(reflected_ray, iter - 1, env);
            }

            return res_color;
        }
    }
}

